extends RigidBody2D

var is_on_floor = false

func _physics_process(delta):
	linear_velocity.x = 200

func _input(event):
	if event.is_action("ui_up") and is_on_floor:
		linear_velocity.y = -600


func floor_entered(body):
	if body.name == 'Floor':
		is_on_floor = true
		$AnimatedSprite.animation = "default"


func floor_exited(body):
	if body.name == 'Floor':
		is_on_floor = false
		$AnimatedSprite.animation = "jump"
